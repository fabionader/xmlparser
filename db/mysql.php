<?php
class DB_MySQL extends PDO
{
	const DRIVER = 'mysql';
	
	protected $dsn = 'mysql:dbname=sigy_vaniasene;host=localhost';
	protected $host = 'localhost';
	protected $db = 'sigy_vaniasene';  
	protected $user = 'novaera';
	protected $password = 'studio';
	
	protected $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'');
	
	public function __construct()
	{
		try {
				parent::__construct($this->dsn, $this->user, $this->password, $this->options);
		} catch (PDOException $e) 
		{
			echo 'Connection failed: ' . $e->getMessage();
		}
	}
}