<?php
	
ini_set("display_errors", 1);
ini_set("log_errors", 1);
header ('Content-type: text/html; charset=UTF-8');
date_default_timezone_set('America/Sao_Paulo');
error_reporting(E_ALL & ~E_NOTICE);
set_time_limit(0);

// if ( !$_GET['importar'] )
// 	exit ('$_GET[]...');

require_once 'parser.php';

$parser = Parser_Factory::factory(Parser_Factory::LOCAL_FILE, array('path' => 'dados/imoveis_sisprof.xml'));
echo date('d-m-Y \à\s H:i:s')." - iniciando...<br>";
?>
<br>
<div id="log">
<?php 
$parser->XMLProcess();
?></div><br>
<?php 
echo date('d-m-Y \à\s H:i:s')." - Finalizado";