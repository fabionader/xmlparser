<?php
ini_set("display_errors", 0);
ini_set("log_errors", 1);
// error_reporting(E_ALL & ~E_NOTICE);
define(ROOT,$_SERVER['DOCUMENT_ROOT']);

require_once 'db/mysql.php';

function imageLoad($cod,$principal=0)
{
	global $db;
	//global $fotos;
	$fotos = array();
	$path = '/fotos'; 
	
	if ( !$db instanceof DB_MySQL )
		$db = new DB_MySQL();
	
	if ( !$cod )
		return;
	
	if ( !isset($fotos[$cod]) )
	{
		$stmt = $db->prepare('SELECT * FROM sg_fotos WHERE id_imovel = ?');
		$stmt->execute(array($cod));
		$res = $stmt->fetchAll(DB_MySQL::FETCH_OBJ);
		$fotos[$cod] = array();
		
		if ( !$res )
			return; //sem imagem
		
		foreach ( $res as $r )
		{
			if ( $r->principal )
				$fotos[$cod]['principal'] = (array) $r;
			
			$fotos[$cod][] = (array) $r;
		}

		if ( !$fotos[$cod]['principal'] )
			$fotos[$cod]['principal'] = $fotos[$cod][0];
		
		if ( $principal )
		{
			if ( file_exists(ROOT . $path . '/' . $cod . '/' . $fotos[$cod]['principal']['FOTO']) )
				return array ( 'path' => $path . '/' . $cod . '/' . $fotos[$cod]['principal']['FOTO'] , 
							   'legenda' => $fotos[$cod]['principal']['legenda']);
			else 
				return array( 'path' => downloadImage($fotos[$cod]['principal']['urloriginal']
													 , $path . '/' . $cod . '/' . $fotos[$cod]['principal']['FOTO']),
							  'legenda' => $fotos[$cod]['principal']['legenda'] );
		} else 
		{
			$fts = $fotos[$cod];
			unset ($fts['principal']);
			$return = array();
			
			foreach ( $fts as $foto )
			{
				if ( file_exists(ROOT . $path . '/' . $cod . '/' . $foto['FOTO']) )
					$return[]['FOTO'] = $path . '/' . $cod . '/' . $foto['FOTO'];
				else 
					$return[]['FOTO'] = downloadImage($foto['urloriginal'], $path . '/' . $cod . '/' . $foto['FOTO']);
			}
			return $return;
		}
	}
}

function downloadImage($source,$dest)
{
	if ( ini_get('allow_url_fopen') == 1 )
	{
		$url = $source;
		$img = ROOT . $dest;
		file_put_contents($img, file_get_contents($url));
	} else
	{
		try {
			@mkdir(dirname(ROOT.$dest),0777);
			
			$fp = fopen (ROOT.$dest, 'wb');              // open file handle
			$ch = curl_init($source);
			// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // enable if you want
			@curl_setopt($ch, CURLOPT_FILE, $fp);          // output to file
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 1000);      // some large value to allow curl to run for a long time
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
			// curl_setopt($ch, CURLOPT_VERBOSE, true);   // Enable this line to see debug prints
			curl_exec($ch);
			
			curl_close($ch);                              // closing curl handle
			fclose($fp);                                  // closing file handle
		} catch (Exception $e)
		{
			return;
		}
	}

	if ( file_exists(ROOT . $dest) )
		return $dest;
	else
		return $source;
}
