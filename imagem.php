<?php
// ini_set("display_errors", 1);
// ini_set("log_errors", 1);

//require_once 'db/mysql.php';

function imageLoad($cod,$principal=0)
{
	global $db;
	global $fotos;
	$path = 'fotos'; 
	
	if ( !$db )
		$db = new DB_MySQL();
	
	if ( !$cod )
		return;
	
	if ( !$fotos[$cod] )
	{
		$stmt = $db->prepare('SELECT * FROM sg_fotos WHERE id_imovel = ?');
		$stmt->execute(array($cod));
		$res = $stmt->fetchAll(DB_MySQL::FETCH_OBJ);
		$fotos[$cod] = array();
		
		if ( !$res )
			return; //sem imagem
		
		foreach ( $res as $r )
		{
			if ( $r->principal )
				$fotos[$cod]['principal'] = (array) $r;
			
			$fotos[$cod][] = (array) $r;
		}

		if ( $principal )
		{
			if ( file_exists($path . '/' . $cod . '/' . $fotos[$cod]['principal']['FOTO']) )
				return $path . '/' . $cod . '/' . $fotos[$cod]['principal']['FOTO'];
			else 
				return downloadImage($fotos[$cod]['principal']['urloriginal'], $path . '/' . $cod . '/' . $fotos[$cod]['principal']['FOTO']);
		} else 
		{
			$fts = $fotos[$cod];
			unset ($fts['principal']);
			$return = array();
			
			foreach ( $fts as $foto )
			{
				if ( file_exists($path . '/' . $cod . '/' . $fts['FOTO']) )
					$return[]['FOTO'] = $path . '/' . $cod . '/' . $fts['FOTO'];
				else 
					$return[]['FOTO'] = downloadImage($fts['urloriginal'], $path . '/' . $cod . '/' . $fts['FOTO']);
			}
			return $return;
		}
	}
}

function downloadImage($source,$dest)
{
	
	if ( ini_get('allow_url_fopen') == 1 )
	{
		$url = $source;
		$img = $dest;
		file_put_contents($img, file_get_contents($url));
	} else
	{
		$ch = curl_init($source);
		$fp = fopen($dest, 'wb');
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);
	}

	if ( file_exists($dest) )
		return $dest;
	else
		return $source;
}
