<?php
require_once 'driver/localfile.php';

class Parser_Factory
{
	const FTP = 'ftp';

	const URL = 'url';

	const LOCAL_FILE = 'local';

	/**
	 * 
	 * @param string $type
	 * @param array $config
	 * @throws Exception
	 * @return Parser_Driver_Abstract
	 */
	public static function factory ($type,$config=array())
	{
		switch ($type)
		{
			case self::FTP :
// 				$conn = new Parser_Driver_FTP($config);
			break;
			
			case self::URL :
// 				$conn = new Parser_Driver_Url($config);
			break;
			
			case self::LOCAL_FILE :
				/*
				 * $config = array('path' => 'path/to/local/file.xml');
				 */
				$conn = new Parser_Driver_Localfile($config);
			break;
			
			default:
				throw new Exception('É necessário selecionar um tipo válido');
			break;
		}
		
		return $conn;
	}
}
