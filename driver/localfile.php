<?php
require_once 'Abstract.php';
class Parser_Driver_Localfile extends Parser_Driver_Abstract
{
	protected $file;
	
	protected $flagDelete = false;
	
	public function init ($config)
	{
		if ( !$config['path'] )
			throw new Exception('Nenhum arquivo passado');
		
		if ( !file_exists($config['path']) )
			throw new Exception('Arquivo passado não existe ou não foi localizado');

		if ( substr($config['path'], $start) )
				
		$this->file = $config['path'];
		
		$this->validateFile($this->file);
	}
	
	private function validateFile ($file)
	{
		$ext = strtolower(substr($file,-3));
		
		switch ($ext) 
		{
			case 'xml':
				return;
			break;
			
			case 'zip':
				$zip = new ZipArchive();
				
				if ( $zip->open($file) === TRUE )
				{
					$dirname = basename($file,'.zip');
					$path = dirname($this->file).DIRECTORY_SEPARATOR.$dirname.DIRECTORY_SEPARATOR;
					
					if ( !is_dir($path) )
						@mkdir($path,0777);
					
					@chmod($path,0777);
					
					$zip->extractTo($path);
					$zip->close();
				
					umask();

					$dir = scandir($path);
					
					foreach ($dir as $f)
					{
						if ( $f == '.' || $f == '..' )
							continue;
						
						if ( strtolower(substr($f,-3)) == 'xml' && substr($f, 0,1) != '.' )
						{
							$this->file = realpath($path . $f);
							$this->flagDelete = true;
							return;
						}
					}
					
					throw new Exception("Não foi há um arquivo XML para importação no ZIP");
					
				} else
				{
					throw new Exception("Não foi possível descompactar o zip '{$this->file}'. Arquivo possivelmente corrompido");
				}
				return;
			break;
			
			default:
				throw new Exception('Formato de arquivo incorreto. Apenas XML ou ZIP');
			break;
		}
	}
	
	public function get_file()
	{
		return $this->file;
	}
	
	public function __destruct()
	{
		if ( $this->flagDelete === true )
		{
			$info = pathinfo($this->file);
			$dir = $info['dirname'];
			
			unlink($this->file);
			
			if ( substr(ltrim($dir,DIRECTORY_SEPARATOR), -5) != 'dados' )
			{
				array_map('unlink', glob($dir . DIRECTORY_SEPARATOR . "*."));
				@rmdir($dir);
			}
		}
	}
	
}