<?php
require_once 'db/mysql.php';

abstract class Parser_Driver_Abstract
{
	protected $defaultEncoding = 'ISO-8859-1';

	protected $parser;

	protected $xml;

	protected $depth;

	protected $closeTag;

	protected $inTag;

	/**
	 *
	 * @var DB_MySQL
	 */
	protected $db;

	protected $preProcess = false;

	protected $posProcess = false;

	protected $data = array();

	protected $fieldMap = array(
				'CodigoImovel' 		  => array ('column' => 'codigo', 'type' => 'alnum'),
				'Publicar'			  => array ('column' => 'publicar', 'type' => 'int'),
				'TipoImovel'   		  => array ('column' => 'categoria', 'type' => 'CATEGORIA'),
				'SubTipoImovel'		  => array ('column' => 'sub_tipo_imovel', 'type' => 'text'),
				'Finalidade'   		  => array ('column' => 'finalidade', 'type' => 'text'),
				'TituloImovel' 		  => array ('column' => 'titulo_imovel', 'type' => 'text'),
				'CategoriaImovel' 	  => array ('column' => 'categoria_imovel', 'type' => 'text'),
				'Estado'			  => array ('column' => 'estado', 'type' => 'alnum'),
				'Cidade'			  => array ('column' => 'cidade', 'type' => 'text'),
				'Bairro'			  => array ('column' => 'bairro', 'type' => 'text'),
				'latitude'			  => array ('column' => 'latitude', 'type' => 'text'),
				'longitude'			  => array ('column' => 'longitude', 'type' => 'text'),
				'NomeCondominio'	  => array ('column' => 'nome_condominio', 'type' => 'text'),
				'TipoOferta'		  => array ('column' => 'tipo_oferta', 'type' => 'int'),
				'PrecoVenda'		  => array ('column' => 'preco_venda', 'type' => 'float'),
				'PrecoMedioM2Venda'   => array ('column' => 'preco_medio_m2_venda', 'type' => 'float'),
				'PrecoIptu'			  => array ('column' => 'preco_iptu', 'type' => 'float'),
				'PrecoLocacao'		  => array ('column' => 'preco_locacao', 'type' => 'float'),
				'PrecoMedioM2Locacao' => array ('column' => 'preco_medio_m2_locacao', 'type' => 'float'),
				'PrecoCondominio'	  => array ('column' => 'preco_condominio', 'type' => 'float'),
				'AreaUtil'			  => array ('column' => 'area_util', 'type' => 'float'),
				'AreaTotal'			  => array ('column' => 'area_total', 'type' => 'float'),
				'UnidadeMetrica'	  => array ('column' => 'unidade_metrica', 'type' => 'alnum'),
				'QtdDormitorios'      => array ('column' => 'qtd_dormitorios', 'type' => 'int'),
				'QtdSuites'			  => array ('column' => 'qtd_suites', 'type' => 'int'),
				'QtdBanheiros'		  => array ('column' => 'qtd_banheiros', 'type' => 'int'),
				'QtdSalas'			  => array ('column' => 'qtd_salas', 'type' => 'int'),
				'QtdVagas'		      => array ('column' => 'qtd_vagas', 'type' => 'int'),
				'QtdAndar'			  => array ('column' => 'qtd_andar', 'type' => 'int'),
				'AnoConstrucao'		  => array ('column' => 'ano_construcao', 'type' => 'int'),
				'Observacao'		  => array ('column' => 'observacao', 'type' => 'text'),
				'LinkVideo'			  => array ('column' => 'link_video', 'type' => 'alnum'),
				'Promocao'			  => array ('column' => 'promocao', 'type' => 'bool'),
				'AreaComum'			  => array ('column' => 'area_comum', 'type' => 'float'),
				'AreaPrivativa'		  => array ('column' => 'area_privativa', 'type' => 'float'),
				'AnoReforma'		  => array ('column' => 'ano_reforma',  'type' => 'int'),
				'NumeroAndar'		  => array ('column' => 'numero_andar', 'type' => 'int'),
				'DataCadastro'	  	  => array ('column' => 'data_cadastro', 'type' => 'date'),
				'DataAtualizacao' 	  => array ('column' => 'data_atualizacao', 'type' => 'datetime')
	);
	
	protected $fieldCaracteristica = array (
			'ArmarioCozinha',
			'Churrasqueira',
			'AreaServico',
			'PortaoEletronico',
			'PisoBloquete',
			'PisoCeramica',
			'Copa',
			'ContraPiso',
			'PisoPorcelanato',
			'ArCondicionado',
			'QtdElevador',
			'Piscina',
			'Sauna',
			'ArmarioDormitorio',
			'TVCabo',
			'Lavabo',
			'ArmarioAreaServico',
			'PisoLaminado',
			'WCEmpregada',
			'PisoGranito',
			'ArmarioBanheiro',
			'ArmarioDormitorioEmpregada',
			'PisoTacoMadeira',
			'Agua',
			'Esgoto',
			'Quintal',
			'RuaAsfaltada',
			'Varanda',
			'Despensa',
			'EnergiaEletrica',
			'Deposito',
			'VarandaGourmet',
			'Escritorio',
			'Vestiario',
			'Terraco',
			'Solarium',
			'PisoArdosia',
			'ArmarioCloset',
			'QtdVagasDescobertas',
			'ArmarioSala',
			'ArmarioHomeTheater',
			'CarpeteAcrilico',
			'JardimInverno',
			'Adega',
			'Mezanino',
			'Hidromassagem',
			'DormitorioReversivel',
			'CarpeteMadeira',
			'ArmarioEscritorio',
			'PeDireitoDuplo',
			'CimentoQueimado',
			'CarpeteNylon',
			'EntradaCaminhoes',
			'PisoElevado',
			'Carpete',
			'ArmarioCorredor',
			'PisoMarmore',
			'Repasse',
			'PisoAquecido',
			'CampoFutebol',
			'PisoEmborrachado',
			'QuadraPoliEsportiva',
			'Caseiro',
			'ServicoCozinha',
			'DormitorioEmpregada',
			'Zelador',
			'Sacada',
			'StatusComercial',
			'CondominioFechado',
			'qtdelevadores'			
			);

	protected $catImovel = array();
	
	protected $photoPath = '../foto';
	
	protected $photoCount = 0;
	
	public function __construct($config)
	{
		$this->init($config);
		
		if ( !is_dir($this->photoPath) )
			mkdir($this->photoPath,0777);

		$this->xml_parser = xml_parser_create($this->defaultEncoding);
		xml_parser_set_option($this->xml_parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($this->xml_parser, XML_OPTION_SKIP_WHITE, 1);
		xml_set_processing_instruction_handler($this->xml_parser, array($this,"pi_handler"));
		xml_set_default_handler($this->xml_parser, array($this,"parseDEFAULT"));
		xml_set_element_handler($this->xml_parser, array($this,"startElement"), array($this,"endElement"));
		xml_set_character_data_handler($this->xml_parser, array($this,"contents"));
	}

	abstract public function init ($config);

	public function startElement($parser, $name, $attrs)
	{
// 		$padTag = str_repeat(str_pad(" ", 3), $this->depth);

		// if (!($inTag == "")) {
		//     $xml .= ">";
		// }
		$this->xml .= "<$name";
// 		foreach ($attrs as $key => $value) {
// 		$this->xml .= "$padTag".str_pad(" ", 3);
// 				$this->xml .= " $key=\"$value\"";
// 		}

		if ( substr($this->xml, -1) !== ">" )
			$this->xml .= '>';
		else
			exit (substr($this->xml, -1));
		$this->inTag = $name;
		$this->depth++;
	}

	public function endElement($parser, $name)
	{
		$this->depth--;

		if ($this->closeTag == TRUE)
		{
			$this->xml .= "</$name>";
			if ( $name == 'Imovel' )
				$this->_process();

			$this->inTag = "";
   		} elseif ($this->inTag == $name)
   		{
       		$this->inTag = "";
		} else
		{
			//$padTag = str_repeat(str_pad(" ", 3), $this->depth);
			$this->xml .= "</$name>";
		}
	}

	public function contents($parser, $data)
	{
		$data = preg_replace("/^\s+/", "", $data);
		$data = preg_replace("/\s+$/", "", $data);

		if (!($data == ""))
		{
			$this->xml .= '<![CDATA['.utf8_encode("$data").']]>';
// 			$this->xml .= utf8_encode("$data");
			$this->closeTag = TRUE;
		} elseif ( in_array($this->inTag, array('Imoveis','Imovel','Carga', 'Fotos','Corretor','Foto')) )
		{
			$this->closeTag = FALSE;
		} else {
			$this->closeTag = TRUE;
		}
	}

	public function parseDEFAULT($parser, $data)
	{
		//$data = preg_replace("/</", "&lt;", $data);
		//$data = preg_replace("/>/", "&gt;", $data);
		$this->xml .= $data;
	}

	public function pi_handler($parser, $target, $data)
	{
		$this->xml .= "<?$target $data?>";
	}

	protected function _parser ()
	{
		$file = $this->get_file();
		if ( !($fp = fopen($file, "r")) )
		{
		    if ( !xml_parse($this->xml_parser, $data, feof($fp)) )
		    {
		       throw new Exception( sprintf("XML error: %s at line %d",
		                            xml_error_string(xml_get_error_code($this->xml_parser)),
		                            xml_get_current_line_number($this->xml_parser)));
		    }
		}
		$this->log('Lendo o XML');
		$this->preProcess();
		while ($data = fread($fp, 4096))
		{
		    if (!xml_parse($this->xml_parser, $data, feof($fp))) {
		       die( sprintf("XML error: %s at line %d",
		                            xml_error_string(xml_get_error_code($this->xml_parser)),
		                            xml_get_current_line_number($this->xml_parser)));
		    }
		}
		$this->posProcess();
		xml_parser_free($this->xml_parser);
	}

	protected function _process ()
	{
		if ( empty($this->xml) )
			throw new Exception('Erro ao processar o arquivo');

		if ( strpos($this->xml,'</Imoveis></Carga>') === false )
			$this->xml .= '</Imoveis></Carga>';

		if ( strpos($this->xml, "<?xml version='1.0'?><Carga><Imoveis>") === false && $this->i > 0 )
			$this->xml = "<?xml version='1.0'?><Carga><Imoveis>".$this->xml;


		if ( !$xml = @simplexml_load_string ($this->xml,NULL,LIBXML_NOCDATA) )
			echo( $this->xml );
		if ( !$xml )
		{
			$this->xml = '';
			return;
		}
		$xml = $xml->Imoveis->Imovel;

		try
		{
		 	$id = $this->_dataInsert($xml);
			$this->_imageProcess($id,$xml->Fotos);
			$this->i++;
			$this->xml = '';
			$this->log(sprintf("%d Imóvel(is) processado(s) e %d imagem(ns) processada(s)",$this->i,$this->photoCount)
					   ,'Insert');
		} catch (Exception $e)
		{
			$this->getConnection()->rollBack();
			exit($e->getMessage());
		}

	}

	protected function preProcess ()
	{
		$db = $this->getConnection();

		$stm = $db->query('SELECT COUNT(*) as t FROM cadimo');
		$res = $stm->fetchAll(DB_MySQL::FETCH_OBJ);
		$res = $res[0];
		
		if ( $res->t > 0 )
		{
			$this->log('Fazendo backup dos dados atuais do banco de dados');
			$result = $db->query(
						'truncate table cadimo_bk;
						 truncate table sg_fotos_bk;
						 truncate table sg_caracteristica_imovel_bk;
						 insert into cadimo_bk select * from cadimo;
						 insert into sg_fotos_bk select * from sg_fotos;
						 insert into sg_caracteristica_imovel_bk select * from sg_caracteristica_imovel;
						 truncate table cadimo;
						 truncate table sg_caracteristica_imovel;
						 truncate table sg_fotos;'
						);
			
			if ( !$result )
				throw new PDOException(print_r($db->errorInfo(),1));
		}
		$db->beginTransaction();
		$this->preProcess = true;
	}

	protected function _dataInsert ($data)
	{
		$db = $this->getConnection();
		$this->data = (array) $data;
		$imovel = array();
		$caracteristicas = array();
		
		foreach ($this->data as $key => $value) 
		{
			$field = $this->normalizeImovel($key, $value);
			
			if ( $field )
				$imovel = array_merge($imovel,$field);
			
			$caracteristica = $this->normalizeCaracteristica($key, $value);
			
			if ( $caracteristica )
				$caracteristicas[] = $caracteristica;
		}
		
		/*if ( in_array($imovel['codigo'],array('809995','2609239','3062431')) )
		{
			//echo $this->xml;
			print_r($data);
		}*/
		
		$status = array();
		
		if ( $imovel['preco_venda'] > 0 )
			$status[] = 'Venda';
		
		if ( $imovel['preco_locacao'] > 0 )
			$status[] = 'Locação';
		
		$imovel['status'] = implode(' e ', $status);
		
		reset($this->data);
		
		$stmt = $db->prepare(
						"REPLACE INTO cadimo (".implode(',',array_keys($imovel)).") 
						VALUES (". implode(',', array_fill(0, count($imovel), '?')) .")"
						);
		try {
			if ( !$stmt->execute(array_values($imovel)) )
			{
				print_r ($imovel);
				exit(print_r($stmt->errorInfo()));
			} else 
			{
				$id = $imovel['codigo'];
			}
			
		} catch(PDOExecption $e) {
			$db->rollback();
			throw new Exception("Error!: " . $e->getMessage() . "</br>");
		}
		
		if ( $id )
		{
			foreach ($caracteristicas as $caracteristica) 
			{
				$caracteristica['id_imovel'] = $id;
				$stmt = $db->prepare(
						"INSERT INTO sg_caracteristica_imovel (".implode(',',array_keys($caracteristica)).")
							VALUES (". implode(',', array_fill(0, count($caracteristica), '?')) .")"
				);
				try {
					$stmt->execute(array_values($caracteristica)) or die (print_r($stmt->errorInfo(),1)); 
				} catch (PDOException $e)
				{
					throw new PDOException($e);
				}
			}
		}  
		
		return $id;
	}

	protected function normalizeImovel ($field,$value)
	{
		if ( !array_key_exists($field, $this->fieldMap) )
			return;
		
		switch ($this->fieldMap[$field]['type'])
		{
			case 'bool':
			case 'float':
			case 'int':
				settype($value,$this->fieldMap[$field]['type']); 
				$item[$this->fieldMap[$field]['column']] = $value;  
			break;
				
			case 'CATEGORIA':
				$cats = $this->getCategorias();
				$k = array_search($value, $cats);
				
				if ( $k ===  false )
				{
					$this->setCategorias($value);
					$cats = $this->getCategorias();
					$k = array_search($value, $cats);
				}
					$item[$this->fieldMap[$field]['column']] = $k;
				
			break;
			
			case 'date':
				$date = strtotime(str_replace('/','-' ,$value));
				$item[$this->fieldMap[$field]['column']] = date('Y-m-d',$date);
			break;
			
			case 'datetime':
				$date = strtotime($value);
				$item[$this->fieldMap[$field]['column']] = date('Y-m-d H:i:s',$date);
			break;
			
			default:
				$item[$this->fieldMap[$field]['column']] = $value;
			break;
		}

		return $item;
	}

	protected function normalizeCaracteristica ($key,$value)
	{
		if ( in_array($key,$this->fieldCaracteristica) ) 
			return array('label' => $key, 'valor' => "{$value}");
	}
	
	public function getCategorias ()
	{
		if ( !sizeof($this->catImovel) )
		{ 
			$db = $this->getConnection();
			$res = $db->query('SELECT * FROM  cadcat');
			$cats = $res->fetchAll(PDO::FETCH_OBJ);
			
			if ( $cats )
			{
				foreach ($cats as $cat) 
				{
					$this->catImovel[$cat->CODIGO] = $cat->CATEGORIA;
				}
			}
			
		}
		
		return $this->catImovel;
	}
	
	public function setCategorias ($val)
	{
		$db = $this->getConnection();
		$stmt = $db->prepare('INSERT INTO cadcat (CATEGORIA) VALUE (?)');
		
		try {
			$res = $stmt->execute(array($val));
			
			if ( !$res )
				exit (print_r($stmt->errorInfo(),1));
			
			$this->catImovel = array();
			
		} catch (PDOException $e)
		{
			throw new PDOException($e);
		}
	}
	
	protected function posProcess ()
	{
		//echo $this->i,"\n";
		$this->log(sprintf("Total de %d Imóvel(is) processado(s) e %d imagem(ns) processada(s)",$this->i,$this->photoCount));
// 		exit(print_r($this->data));
		try {
			$this->getConnection()->commit();
			$this->log("Atualizando banco de dados");
		} catch (Exception $e)
		{
// 			print_r $this->getConnection()->errorInfo();
		}
	}

	protected function _imageProcess ($id,$fotos)
	{
		$fotos = (array) current($fotos);
		
		if ( !count($fotos) )
			return; 
		
		$path  = $this->photoPath. DIRECTORY_SEPARATOR . $id;
		$db = $this->getConnection();
		$sql = 'INSERT INTO sg_fotos (id_imovel,legenda,FOTO,urloriginal,principal,alterada) VALUES ';
		$bind = array();
		/*$stmt = $db->prepare('INSERT INTO sg_fotos (id_imovel,legenda,FOTO,urloriginal,principal,alterada) 
								VALUE (:id,:legenda,:foto,:urloriginal,:principal,:alterada);');
		
		$stmt->bindParam(':id',$id);
		$stmt->bindParam(':legenda',$legenda);
		$stmt->bindParam(':foto',$NomeArquivo);
		$stmt->bindParam(':urloriginal',$URLArquivo);
		$stmt->bindParam(':principal',$Principal);
		$stmt->bindParam(':alterada',$Alterada);*/
		//if ( !is_dir( $path ) )
		//	mkdir($path,0777) or die ('Impossível criar o diretório das imagens');
		
		foreach ( $fotos as $foto )
		{
			$foto = (array)$foto;
// 			$legenda = NULL;
// 			extract($foto);
			
			if ( $foto['NomeArquivo'] != basename($foto['URLArquivo']) )
			{
				$foto['legenda'] = $foto['NomeArquivo'];
				$foto['NomeArquivo'] = basename($foto['URLArquivo']);
			}
			$bind[] = "('". $id . "',"
					. $db->quote($foto['legenda']) . "," 
					. $db->quote($foto['NomeArquivo']) . ",'" 
					. $foto['URLArquivo'] . "','" 
					. $foto['Principal'] . "','" 
					. $foto['Alterada'] . "')"; 
// 			$val = array($id,$foto['NomeArquivo'],$foto['URLArquivo'],$foto['Principal'],$foto['Alterada']);
			
			$this->photoCount++;
		}
		
		return $db->query($sql . implode(" , ", $bind));
// 			$res = $stmt->execute();
		
		/*
		 *
		[Fotos] => SimpleXMLElement Object
		(
				[Foto] => Array
				(
						[0] => SimpleXMLElement Object
						(
								[NomeArquivo] => 7676320810678426A224E308D8F998D80388BA6627AC364FA.jpg
								[URLArquivo] => http://cdn1.valuegaia.com.br/_Fotos/2081/2358/7676320810678426A224E308D8F998D80388BA6627AC364FA.jpg
								[Principal] => 1
								[Alterada] => 0
						)
				)
		)
		
		*/
		
// 		$url = 'http://example.com/image.php';
// 		$img = '/my/folder/flower.gif';
// 		file_put_contents($img, file_get_contents($url));

// 		$ch = curl_init('http://example.com/image.php');
// 		$fp = fopen('/my/folder/flower.gif', 'wb');
// 		curl_setopt($ch, CURLOPT_FILE, $fp);
// 		curl_setopt($ch, CURLOPT_HEADER, 0);
// 		curl_exec($ch);
// 		curl_close($ch);
// 		fclose($fp);
	}

	public function XMLProcess ()
	{
		$this->log('Iniciando processo de importação');
		$this->_parser();
	}

	protected function log($msg=NULL,$event=NULL)
	{
		if ( $msg )
			$this->log[$event] = $msg;
		echo "<script language=\"javascript\">document.getElementById('log').innerHTML='".(end($this->log))."';</script>";
		flush();
	}
	
	/**
	 * @return DB_MySQL $this->db
	 */
	public function getConnection()
	{
		if ( !$this->db )
			$this->db = new DB_MySQL();

		return $this->db;
	}
}
