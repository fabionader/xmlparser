# README #

XMLParser para rodar no Zend Framework 1.x

### Instalação ###

* No bootstrap incluir:

```
#!php
define('XMLPARSER', realpath(dirname(__FILE__) . '/app/models/xmlparser'));

```
* No Admin_ImportController, fazer o include da pasta xmlparser que deve ficar dentro de models:

```
#!php

require_once XMLPARSER .'/parser.php';

```

e setar as configurações e botar pra rodar no indexAction ();

```
#!php

		$parser = Parser_Factory::factory(Parser_Factory::LOCAL_FILE, 
											array(
												  'root' => './',
												  'path' => 'dados/imoveis_sisprof.xml',
												  'db'   => $this->db->getConnection()
								   				)
										);
		echo date('d-m-Y \à\s H:i:s')." - iniciando...<br>";
		echo '<br><div id="log">';
		$parser->XMLProcess();
		echo "</div><br>". date('d-m-Y \à\s H:i:s')." - Finalizado";
		exit;
```